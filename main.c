#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <string.h>
#include "determinant.h"

#define DEBUG 0
#define READFROMFILE 1
#define NUM 100

double* cramersRule(double **A, double *B, int n, int *vars);
double* cramersRuleSerial(double **A, double *B, int n, int *vars);
int readInMatrix();

double **Arr;
double *Ans;
char fileName[150];
int THREAD_COUNT;
 
/* Run ./main threadCount fileName */
int main(int argc, char* argv[]){
   int i, j;
   int size;
   THREAD_COUNT = atoi(argv[1]);
   if(argc == 3){
      strcpy(fileName, argv[2]);
   }
   else{
      printf("Please specify input file\n");
      return 1;
   }
   if(READFROMFILE){
      size = readInMatrix();
   }
   else{
      /* initialize random number generator */
      time_t t;
      srand(21);

      size = NUM;
      Ans = (double *) malloc(size * sizeof(double));
      Arr = (double **) malloc(size * sizeof(double *));
      for(i = 0; i < size; i++){
         Arr[i] = (double *) malloc(size * sizeof(double));
      }

      for(i = 0; i < size; i++){
         for(j = 0; j < size; j++){
            Arr[i][j] = rand() % 143;
         }
         Ans[i] = rand() % 119;
      }
   }
   double start;
   double *Answer;
   double *Answer1;
   double solution;

   if(DEBUG == 1){
      start = omp_get_wtime();
      solution = Determinant(Arr, size);
      printf("Det1: %lf -- time: %lf\n", solution, omp_get_wtime() - start);
      start = omp_get_wtime();
      solution = quickDeterminant(Arr, size);
      printf("Det2: %lf -- time: %lf\n", solution, omp_get_wtime() - start);
      return;
   }

   /* Deciding which variables you want to solve for */
   int *solveFor = (int *) malloc(size * sizeof(int));
   for(i = 0; i < size; i++){
      solveFor[i] = 1;  
   }
   
   start = omp_get_wtime(); 
   Answer = cramersRuleSerial(Arr, Ans, size, solveFor);
   //printf("serial time: %lf\n", omp_get_wtime() - start);
   start = omp_get_wtime(); 
   Answer1 = cramersRule(Arr, Ans, size, solveFor);
   printf("parallel time: %lf\n", omp_get_wtime() - start);

   /* Print the results to answers.txt */
   FILE *fp;
   fp = fopen("answers.txt", "w+");
   for(i = 0; i < size; i++){
      if(solveFor[i] == 1){
        fprintf(fp, "Var%d: %lf\n", i, Answer1[i]); 
      }
   }
   
   free(Answer);
   free(Arr);
   free(Ans);
   free(solveFor);
   return 0;
}

/* Uses Cramer's Rule to solve for the given variable
 * This is the serial version
 * A is the coefficient matrix, B is the answer matrix, n is the matrix size
 * vars is the matrix of variables we want to solve for, a value of 1 indicates that we should solve for it
 */ 
double* cramersRuleSerial(double **A, double *B, int n, int *vars){
   int i,j, x, y;
   long double detA;
   detA = quickDeterminant(A, n);
   /* Cramer's Method doesn't work if the determinant is 0 */
   if(detA == 0){
      printf("Matrix has determinant 0, can't solve\n");
      return NULL;
   }

   /* Create the matrix of determinants */
   double *values = (double *)malloc(n * sizeof(double));
   double **newA = (double **)malloc(n * sizeof(double *)); 
   for(i = 0; i < n; i++){
      newA[i] = (double *) malloc(n * sizeof(double));
      memcpy(newA[i], A[i], n * sizeof(double));
   }

   /* Solve for each value of */
   for(i = 0; i < n; i++){
      if(vars[i] == 1){
         for(j = 0; j < n; j++){
            newA[j][i] = B[j]; 
         }

         values[i] = (double) (quickDeterminant(newA, n) / detA);
         for(j = 0; j < n; j++){
            newA[j][i] = A[j][i];
         }
      }
   }

   for(i = 0; i < n; i++){
      free(newA[i]);
   }
   free(newA);
   return values;
}

/* Reads matrices from the given file 
 * Returns the size of the matrices
 * */
int readInMatrix(){
   FILE *fp;
   int size, i, j;
   fp = fopen(fileName, "r");
   if(fp == NULL){
      printf("ERROR OPENING FILE!");
      exit(1);
   }

   /* Get the size of the array and allocate the arrays */
   fscanf(fp, "%d", &size);
   Ans = (double *) malloc(size * sizeof(double));
   Arr = (double **) malloc(size * sizeof(double *));
   for(i = 0; i < size; i++)
      Arr[i] = (double *) malloc(size * sizeof(double));

   for(i = 0; i < size; i++){
      for(j = 0; j < size; j++){
         fscanf(fp, "%lf", &Arr[i][j]);
      }
   }
   for(i = 0; i < size; i++){
      fscanf(fp, "%lf", &Ans[i]);
   }
   return size;
}



/* Uses Cramer's Rule to solve for the given variable
 * This is the parallel version
 * A is the coefficient matrix, B is the answer matrix, n is the matrix size
 * vars is the matrix of variables we want to solve for, a value of 1 indicates that we should solve for it
 */ 
double* cramersRule(double **A, double *B, int n, int *vars){
   long double detA;
   /* Create the matrix of determinants */
   double *values = (double *)malloc(n * sizeof(double));
   long double *determinants = (long double *)malloc(n * sizeof(long double));
   int i,k,d,j;


   # pragma omp parallel num_threads(THREAD_COUNT) default(none) shared(A, B, n, detA, values, vars, determinants) private(d, j)
   {
   /* Create mutable copy of A */
   double **newA = (double **)malloc(n * sizeof(double *)); 
   for(d = 0; d < n; d++){
      newA[d] = (double *) malloc(n * sizeof(double));
      memcpy(newA[d], A[d], n * sizeof(double));
   }

   /* Calculate all the determinants */
   #pragma omp for schedule(static) nowait
   for(i = -1; i < n; i++){
      if(i == -1){
         detA = quickDeterminant(A, n);
      }
      else if(vars[i] == 1){
         for(j = 0; j < n; j++){
            newA[j][i] = B[j]; 
         }

         determinants[i] = quickDeterminant(newA, n);
         for(j = 0; j < n; j++){
            newA[j][i] = A[j][i];
         }
      }
   }

   /* Free the memory each thread used */
   for(d = 0; d < n; d++){
      free(newA[d]);
   }
   free(newA);

   /* Necessary because the next block of code relies on the determinants being calculated */
   #pragma omp barrier

   /* Calculate the actual values */
   #pragma omp for schedule(static)
   for(i = 0; i < n; i++){
      if(vars[i] == 1 && detA != 0){
         values[i] = (double) (determinants[i] / detA);
      }
   }
   

   } /* End of parallel code */

   return values;
}

