#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>

#define DEBUGNUM 176

long double quickDeterminant(double **a, int n);
long double LUDecomposition(double **a, int n);
double Determinant(double **a,int n);

/* Makes use of LU decomposition to do determinant calculation in O(n^3) time 
 * Link to LU Decomposition Algorithm: https://en.wikipedia.org/wiki/LU_decomposition
 */
long double LUDecomposition(double **a, int n){
   double **lower = (double **) malloc(n * sizeof(double *));
   double **upper = (double **) malloc(n * sizeof(double *));
   int i, j, k, t, x, y;
   double upperVal, lowerVal;
   
   /* Fully create the arrays and fill in the already known values 
    * This loop is fully multi-threadable
    */
   for(i = 0; i < n; i++){
      lower[i] = (double *) malloc(n * sizeof(double));
      upper[i] = (double *) malloc(n * sizeof(double));
      for(j = 0; j < n; j++){
         if(i < j){
            lower[i][j] = 0.0;
         }
         else if(i > j){
            upper[i][j] = 0.0; 
         }
         else{
            lower[i][j] = 1.0;
         }
      }
   }

   /* Filling in the upper matrix */
   for(i = 0; i < n; i++){
      for(j = 0; j < i; j++){
         lowerVal = 0;
         /* Can optimize the continue check in the for statement */
         for(k = 0; k < j; k++){
               lowerVal += upper[k][j] * lower[i][k];
         }
         lower[i][j] = (1.0/upper[j][j])*(a[i][j] - lowerVal);
      }
      for(j = i; j < n; j++){
         upperVal = 0;
         for(k = 0; k < i; k++){
            upperVal += upper[k][j] * lower[i][k];
         }
         upper[i][j] = a[i][j] - upperVal;
      }
   }

   long double det = 1;
   for(i = 0; i < n; i++){
      det *= upper[i][i];
      free(upper[i]);
      free(lower[i]);
   }
   free(upper);
   free(lower);

   return det;
}

long double quickDeterminant(double **a, int n){
   /*
   if(n == 1){
      return a[0][0];
   }
   else if(n == 2){
      return a[0][0] * a[1][1] - a[1][0] * a[0][1];
   }
   else{
      return LUDecomposition(a, n);
   }
   */
   return LUDecomposition(a, n);

}


/* Source: http://paulbourke.net/miscellaneous/determinant/determinant.c
 * Calculates determinates in O(n!) time */
double Determinant(double **a,int n)
{
    int i,j,j1,j2 ;                    // general loop and matrix subscripts
    long double det = 0 ;                   // init determinant
    double **m = NULL ;                // pointer to pointers to implement 2d
                                       // square array

    if (n < 1)    {   }                // error condition, should never get here

    else if (n == 1) {                 // should not get here
        det = a[0][0] ;
        }

    else if (n == 2)  {                // basic 2X2 sub-matrix determinate
                                       // definition. When n==2, this ends the
        det = a[0][0] * a[1][1] - a[1][0] * a[0][1] ;// the recursion series
        }


                                       // recursion continues, solve next sub-matrix
    else {                             // solve the next minor by building a
                                       // sub matrix
        det = 0 ;                      // initialize determinant of sub-matrix

                                           // for each column in sub-matrix
        for (j1 = 0 ; j1 < n ; j1++) {
                                           // get space for the pointer list
            m = (double **) malloc((n-1)* sizeof(double *)) ;

            for (i = 0 ; i < n-1 ; i++)
                m[i] = (double *) malloc((n-1)* sizeof(double)) ;
                       //     i[0][1][2][3]  first malloc
                       //  m -> +  +  +  +   space for 4 pointers
                       //       |  |  |  |          j  second malloc
                       //       |  |  |  +-> _ _ _ [0] pointers to
                       //       |  |  +----> _ _ _ [1] and memory for
                       //       |  +-------> _ a _ [2] 4 doubles
                       //       +----------> _ _ _ [3]
                       //
                       //                   a[1][2]
                      // build sub-matrix with minor elements excluded
            for (i = 1 ; i < n ; i++) {
                j2 = 0 ;               // start at first sum-matrix column position
                                       // loop to copy source matrix less one column
                for (j = 0 ; j < n ; j++) {
                    if (j == j1) continue ; // don't copy the minor column element

                    m[i-1][j2] = a[i][j] ;  // copy source element into new sub-matrix
                                            // i-1 because new sub-matrix is one row
                                            // (and column) smaller with excluded minors
                    j2++ ;                  // move to next sub-matrix column position
                    }
                }

            det += pow(-1.0,1.0 + j1 + 1.0) * a[0][j1] * Determinant(m,n-1) ;
                                            // sum x raised to y power
                                            // recursively get determinant of next
                                            // sub-matrix which is now one
                                            // row & column smaller

            for (i = 0 ; i < n-1 ; i++) free(m[i]) ;// free the storage allocated to
                                            // to this minor's set of pointers
            free(m) ;                       // free the storage for the original
                                            // pointer to pointer
        }
    }
    return(det) ;
}

