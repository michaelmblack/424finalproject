CC=gcc
CFLAGS=-fopenmp -lm

make:
	$(CC) -o main main.c determinant.c $(CFLAGS)

debug:
	$(CC) -o debugVer main.c determinant.c $(CFLAGS) -g
