#ifndef _DETERMINANT_H_
#define _DETERMINANT_H_
#include <omp.h>

double Determinant(double **a, int n);
long double quickDeterminant(double **a, int n);
long double LUDecomposition(double **a, int n);

#endif
