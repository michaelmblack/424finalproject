import numpy as np
myFileName = "Test2.txt"

def readInArrays():
  multBy = 1
  myFile = open(myFileName, 'r')
  size = int(myFile.readline())
  print size
  A = np.eye(size)
  for i in range(size):
    print "next"
    line = myFile.readline().replace(" ", "")
    line.strip()
    t = 0
    for char in line:
      if char != '\n':
        if char != '-':
          A[i][t] = multBy * int(char)
          t += 1
          multBy = 1
        else:
          multBy = -1
  answers = myFile.readline().replace(" ", "")
  answers.strip()
  B = list()
  for char in answers:
      if char != '\n':
        if char != '-':
          B.append(multBy * int(char))
          multBy = 1
        else:
          multBy = -1


  myFile.close()
  print A
  print B
  return (A, B, size)

arraysTuple = readInArrays()
print np.linalg.solve(arraysTuple[0], np.array(arraysTuple[1]))
